# -*- coding: utf-8 -*-

from jojo.kanji import get_kanji, get_kanjis

def test_get_first_kanji():
    # Given
    kanji_number = 0

    # When
    kanji = get_kanji(kanji_number)

    # Then
    assert kanji == u'年'

def test_get_hundredth_kanji():
    # Given
    kanji_number = 99

    # When
    kanji = get_kanji(kanji_number)

    # Then
    assert kanji == u'北'

def test_get_first_ten_kanjis():
    # Given
    kanji_idx = 10

    # When
    kanjis = get_kanjis()[:kanji_idx]

    # Then
    assert kanjis == [u'年', u'日', u'月', u'大', u'本', u'学', u'人', u'国', u'中', u'一']