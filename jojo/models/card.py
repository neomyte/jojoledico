from genanki import Model, Note
from jojo.infra.jisho import search_word, search_kanji

class WordCard(Note):
    anki_model = Model(
        66664,
        'Jojo Le Dico Model',
        fields=[
            {'name': 'Word'},
            {'name': 'Reading'},
            {'name': 'Entry'}
        ],
        templates=[
            {
                'name': 'Le jeu',
                'qfmt': \
                    '<center>'\
                        '<p style="font-size:60px">{{ Word }}</p>'\
                    '</center>',
                'afmt': \
                    '<center>'\
                        '<div style="font-size:60px">'\
                            '<h1>{{ FrontSide }}</h1>'\
                        '</div>'\
                        '<hr>'\
                        '<div style="font-size:40px">'\
                            '<h1>Reading</h1>'\
                            '{{ Reading }}<hr>'\
                            '<h1>Jisho\'s entry</h1>'\
                            '{{ Entry }}'\
                        '</div>'\
                    '</center>'
            },
        ]
    )

    def __init__(self, word):
        super().__init__(
            model  = WordCard.anki_model,
            fields = search_word(word)
        )

class KanjiCard(Note):
    anki_model = Model(
        66663,
        'Jojo Le Dico Model',
        fields=[
            {'name': 'Kanji'},
            {'name': 'Reading'},
            {'name': 'Entry'}
        ],
        templates=[
            {
                'name': 'Le jeu',
                'qfmt': \
                    '<center>'\
                        '<p style="font-size:60px">{{ Kanji }}</p>'\
                    '</center>',
                'afmt': \
                    '<center>'\
                        '<div style="font-size:60px">'\
                            '<h1>{{ FrontSide }}</h1>'\
                        '</div>'\
                        '<hr>'\
                        '<div style="font-size:40px">'\
                            '<h1>Reading</h1>'\
                            '{{ Reading }}<hr>'\
                            '<h1>Jisho\'s entry</h1>'\
                            '{{ Entry }}'\
                        '</div>'\
                    '</center>'
            },
        ]
    )

    def __init__(self, word):
        super().__init__(
            model  = KanjiCard.anki_model,
            fields = search_kanji(word)
        )
