import json
from urllib.request import urlopen
from urllib.parse import quote as urlquote
from bs4 import BeautifulSoup

def senses_to_html(senses):
    return '<ul>' + '<li>'.join(
        ', '.join(
            sense['english_definitions']
        )
        for sense in senses
    ) + '</ul>'

def parse_word(data):
    if 'data' not in data:
        print(data)
        return ['', '', '']
    return [
        data['data'][0]['japanese'][0]['reading'],
        data['data'][0]['japanese'][0].get('word', data['data'][0]['japanese'][0]['reading']),
        senses_to_html(data['data'][0]['senses'])
    ]

def reading_to_html(kun_readings, on_readings):
    return '<ul>' + '<li>'.join(kun_readings) + '</ul><ul>'+ '<li>'.join(on_readings) + '</ul>'

def kanji_meanings_to_html(meanings):
    return '<ul>' + '<li>'.join(meanings) + '</ul>'

def parse_kanji(data):
    if 'data' not in data:
        #print(data)
        return ['', '', '']
    return [
        reading_to_html(
            [child.text for child in data.find_all("dt")[3].find_next().find_all('a')],
            [child.text for child in data.find_all("dt")[4].find_next().find_all('a')],
        ),
        data.find_all("h1")[1].text,
        kanji_meanings_to_html(data.body.find(class_='kanji-details__main-meanings').text.strip().split(', '))
    ]


def search_word(word):
    print(f'https://jisho.org/api/v1/search/words?keyword={urlquote(word)}')
    with urlopen(f'https://jisho.org/api/v1/search/words?keyword={urlquote(word)}') as f:
        return parse_word(json.loads(f.read().decode('utf-8')))

def search_kanji(kanji):
    print(f'https://jisho.org/search/{urlquote(kanji)}')
    with urlopen(f'https://jisho.org/search/{urlquote(kanji)}') as f:
        return parse_kanji(BeautifulSoup(f.read().decode('utf-8'), 'html.parser'))