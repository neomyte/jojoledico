# -*- coding: utf-8 -*-

import json

words = []

def get_words():
    global words
    if not words:
        with open('data/words.json') as k_f:
            words = json.load(k_f)
    return words


def get_word(number):
    return get_words()[number]
