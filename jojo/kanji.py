# -*- coding: utf-8 -*-

import json

kanjis = []

def get_kanjis():
    global kanjis
    if not kanjis:
        with open('data/kanjis.json') as k_f:
            kanjis = json.load(k_f)
    return kanjis


def get_kanji(number):
    return get_kanjis()[number]
