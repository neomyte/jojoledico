from genanki import Deck, Package
from jojo.models.card import KanjiCard, WordCard
from jojo.kanji import get_kanjis
from jojo.word import get_words

def jojo():
    deck = Deck(43, 'Jojo Cards')
    kanjis_met = []
    kanjis = get_kanjis()
    for kanji in kanjis:
        kanjis_met.append(kanji)
        deck.add_note(
            KanjiCard(
                kanji
            )
        )
        for word in get_words():
            if all(c in kanjis_met or c not in kanjis for c in word):
                deck.add_note(
                    WordCard(
                        word
                    )
                )
    Package(deck).write_to_file('test.apkg')